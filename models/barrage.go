package models

import (
	"database/sql"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"time"
)

func SaveBarrage(content, uid, episodesId, videoId, currentTime string) error {
	stmt, err := db.Prepare("INSERT INTO barrage (content, add_time, user_id, status, `current_time`, episodes_id, video_id) VALUES (?,?,?,?,?,?,?) ")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return err
	}
	_, err = stmt.Exec(content, time.Now().Unix(), uid, 1, currentTime, episodesId, videoId)
	if err != nil {
		golf.Critical(err)
		return err
	}
	return nil
}
func GetBarrage(episodesId int, startTime int, endTime int) ([]*entity.Barrage, error) {
	stmt, err := db.Prepare("SELECT id, content, `current_time` FROM barrage WHERE status = 1 AND episodes_id = ? AND `current_time` >= ? AND  `current_time` <= ? ORDER BY `current_time` ASC")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(episodesId, startTime, endTime)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	barrages := make([]*entity.Barrage, 0)
	for rows.Next() {
		var id int
		var content string
		var currentTime int
		rows.Scan(&id, &content, &currentTime)
		barrages = append(barrages, &entity.Barrage{Id: id, Content: content, CurrentTime: currentTime})
	}
	return barrages, nil
}
