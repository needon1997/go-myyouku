package models

import (
	"database/sql"
	"github.com/gomodule/redigo/redis"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"strings"
	"time"
)

func GetUserInfo(userId string) (*entity.UserInfo, error) {
	userinfo := &entity.UserInfo{}
	rediskey := "user:info:" + userId
	exist, err := redis.Bool(rdb.Do("exists", rediskey))
	if exist {
		res, _ := redis.Values(rdb.Do("hgetall", rediskey))
		redis.ScanStruct(res, userinfo)
		return userinfo, nil
	}
	stmt, err := db.Prepare("SELECT id, name, add_time, avatar FROM user WHERE id = ?")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	err = stmt.QueryRow(userId).Scan(&userinfo.Id, &userinfo.Name, &userinfo.AddTime, &userinfo.Avatar)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	_, err = rdb.Do("hmset", redis.Args{rediskey}.AddFlat(userinfo)...)
	if err == nil {
		rdb.Do("expire", rediskey, 86400)
	}
	return userinfo, nil
}
func UserSave(mobile, password string) error {
	stmt, err := db.Prepare("INSERT INTO user (name,mobile,password,status,add_time,avatar) VALUES (?,?,?,?,?,?)")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return err
	}
	name := ""
	status := 1
	ctime := time.Now().Unix()
	avatar := ""
	_, err = stmt.Exec(name, mobile, password, status, ctime, avatar)
	if err != nil {
		golf.Critical(err)
		return err
	}
	return nil
}
func UserLogin(mobile, password string) (*entity.User, error) {
	stmt, err := db.Prepare("SELECT id, name, add_time FROM user WHERE mobile = ? AND password = ?")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	var name string
	var id int64
	var ctime int64

	err = stmt.QueryRow(mobile, password).Scan(&id, &name, &ctime)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	return &entity.User{Id: id, Name: name, Mobile: mobile, AddTime: ctime}, nil
}
func SearchMobile(mobile string) (bool, error) {
	stmt, err := db.Prepare("SELECT mobile FROM user WHERE mobile = ?")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return false, err
	}
	var m string
	err = stmt.QueryRow(mobile).Scan(&m)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		golf.Critical(err)
		return false, err
	}
	return true, nil

}

func SaveMessage(content string) (int64, error) {
	stmt, err := db.Prepare("INSERT INTO message (content,ctime) VALUES(?,?);")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return -1, err
	}
	ctime := time.Now().Unix()
	result, err := stmt.Exec(content, ctime)
	if err != nil {
		golf.Critical(err)
		return -1, err
	}
	return result.LastInsertId()
}

func ConsumeSendMessage() {
	go Consume("", "message", func(msgData string) error {
		uid_mid := strings.Split(msgData, ":")
		uid := uid_mid[0]
		mid := uid_mid[1]
		stmt, err := db.Prepare("INSERT INTO message_user  (user_id, message_id, status, add_time) VALUES (?,?,?,?)")
		if err != nil {
			golf.Critical(err)
			return err
		}
		_, err = stmt.Exec(uid, mid, 1, time.Now().Unix())
		if err != nil {
			golf.Critical(err)
			return err
		}
		return nil
	})
}
