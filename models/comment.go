package models

import (
	"database/sql"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"time"
)

func GetEpisodeComment(episodeId, limit, offset string) ([]*entity.CommentInfo, error) {
	stmt, err := db.Prepare("SELECT id, content, add_time, user_id, stamp, praise_count FROM comment WHERE episodes_id = ? AND status = 1 ORDER BY add_time DESC LIMIT ? OFFSET ? ")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(episodeId, limit, offset)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	var Id int64
	var Content string
	var AddTime int64
	var UserId int
	var Stamp int
	var PraiseCount int

	commentList := make([]*entity.CommentInfo, 0)
	for rows.Next() {
		rows.Scan(&Id, &Content, &AddTime, &UserId, &Stamp, &PraiseCount)
		commentList = append(commentList, &entity.CommentInfo{Id: Id, Content: Content, AddTime: AddTime, UserId: UserId, Stamp: Stamp, PraiseCount: PraiseCount})
	}
	return commentList, nil
}

func SaveComment(content, uid, episodesId, videoId string) error {
	stmt, err := db.Prepare("INSERT INTO comment (content, add_time, user_id, status, stamp, praise_count, episodes_id,video_id) VALUES (?,?,?,?,?,?,?,?) ")
	if err != nil {
		golf.Critical(err)
		return err
	}
	addTime := time.Now().Unix()
	stamp := 0
	status := 1
	praiseCount := 0
	_, err = stmt.Exec(content, addTime, uid, status, stamp, praiseCount, episodesId, videoId)
	if err != nil {
		golf.Critical(err)
		return err
	}
	go Publish("", "video:comment", videoId)
	go Publish("", "episodes:comment", episodesId)
	return nil
}

func ConsumeComment() {
	go Consume("", "video:comment", func(videoId string) error {
		stmt, err := db.Prepare("UPDATE video SET comment = comment +1 WHERE id = ? ")
		defer func() {
			stmt.Close()
		}()
		if err != nil {
			golf.Critical(err)
			return err
		}
		_, err = stmt.Exec(videoId)
		if err != nil {
			golf.Critical(err)
			return err
		}
		return nil
	})
	go Consume("", "episodes:comment", func(episodesId string) error {
		stmt, err := db.Prepare("UPDATE video_episodes SET comment = comment +1 WHERE id = ? ")
		defer func() {
			stmt.Close()
		}()
		if err != nil {
			golf.Critical(err)
			return err
		}
		_, err = stmt.Exec(episodesId)
		if err != nil {
			golf.Critical(err)
			return err
		}
		return nil
	})
}
