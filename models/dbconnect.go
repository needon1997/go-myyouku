package models

import (
	"database/sql"
	"github.com/gomodule/redigo/redis"
	golf "github.com/needon1997/go-golf"
)

import _ "github.com/go-sql-driver/mysql"

var db *sql.DB
var rdb redis.Conn

func init() {
	conn, err := sql.Open("mysql", golf.App.Config.String("DB_URL"))
	if err != nil {
		panic("DB connection failure, program exit")
	}
	err = conn.Ping()
	if err != nil {
		panic("DB connection failure, program exit")
	}
	golf.Info("DB connection success")
	conn.SetMaxIdleConns(100)
	conn.SetMaxOpenConns(50)
	db = conn

	c, err := redis.Dial("tcp", golf.App.Config.String("RDB_URL"))
	if err != nil {
		panic("DB connection failure, program exit")
	}
	rdb = c

}
