package models

import (
	"database/sql"
	"encoding/json"
	"github.com/gomodule/redigo/redis"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"time"
)

func GetChannelAdvert(channelId string) ([]*entity.Advert, error) {
	stmt, err := db.Prepare("SELECT id,title,sub_title,img,add_time,url FROM advert WHERE channel_id = ? AND status = 1")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(channelId)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	adverts := make([]*entity.Advert, 0)
	var id int64
	var title string
	var subTitle string
	var img string
	var ctime int64
	var url string
	for rows.Next() {
		rows.Scan(&id, &title, &subTitle, &img, &ctime, &url)
		adverts = append(adverts, &entity.Advert{Id: id, Title: title, SubTitle: subTitle, Img: img, AddTime: ctime, Url: url})
	}
	return adverts, nil
}

func GetChannelHotList(channelId string, limit int) ([]*entity.Video, error) {
	stmt, err := db.Prepare("SELECT id,title,sub_title,img,img1,add_time,episodes_count,is_end FROM video WHERE channel_id = ? AND is_hot = 1 AND status = 1 LIMIT ?")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(channelId, limit)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	hotVideos := make([]*entity.Video, 0)
	var id int64
	var title string
	var subTitle string
	var img string
	var img1 string
	var ctime int64
	var episodesCount int
	var isEnd int
	for rows.Next() {
		rows.Scan(&id, &title, &subTitle, &img, &img1, &ctime, &episodesCount, &isEnd)
		hotVideos = append(hotVideos, &entity.Video{Id: id, Title: title, SubTitle: subTitle, Img: img, Img1: img1, AddTime: ctime, EpisodesCount: episodesCount, IsEnd: isEnd})
	}
	return hotVideos, nil
}

func GetRecommendRegionList(channelId string, regionId string, limit int) ([]*entity.Video, error) {
	stmt, err := db.Prepare("SELECT id,title,sub_title,img,img1,add_time,episodes_count,is_end FROM video WHERE channel_id = ? AND region_id = ? AND is_recommend = 1 AND status = 1 LIMIT ?")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(channelId, regionId, limit)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	recommendVideos := make([]*entity.Video, 0)
	var id int64
	var title string
	var subTitle string
	var img string
	var img1 string
	var ctime int64
	var episodesCount int
	var isEnd int
	for rows.Next() {
		rows.Scan(&id, &title, &subTitle, &img, &img1, &ctime, &episodesCount, &isEnd)
		recommendVideos = append(recommendVideos, &entity.Video{Id: id, Title: title, SubTitle: subTitle, Img: img, Img1: img1, AddTime: ctime, EpisodesCount: episodesCount, IsEnd: isEnd})
	}
	return recommendVideos, nil
}

func GetRecommendTypeList(channelId string, regionId string, limit int) ([]*entity.Video, error) {
	stmt, err := db.Prepare("SELECT id,title,sub_title,img,img1,add_time,episodes_count,is_end FROM video WHERE channel_id = ? AND type_id = ? AND is_recommend = 1 AND status = 1 LIMIT ?")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(channelId, regionId, limit)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	recommendVideos := make([]*entity.Video, 0)
	var id int64
	var title string
	var subTitle string
	var img string
	var img1 string
	var ctime int64
	var episodesCount int
	var isEnd int
	for rows.Next() {
		rows.Scan(&id, &title, &subTitle, &img, &img1, &ctime, &episodesCount, &isEnd)
		recommendVideos = append(recommendVideos, &entity.Video{Id: id, Title: title, SubTitle: subTitle, Img: img, Img1: img1, AddTime: ctime, EpisodesCount: episodesCount, IsEnd: isEnd})
	}
	return recommendVideos, nil
}

func GetChannelVideoList(channelId, regionId, typeId, end, sort, limit, offset string) ([]*entity.Video, error) {
	conditionStr := " WHERE 1=1 AND status = 1 AND channel_id = ?"
	if regionId != "" {
		conditionStr += " AND region_id = " + regionId
	}
	if typeId != "" {
		conditionStr += " AND type_id = " + typeId
	}
	if end == "n" {
		conditionStr += " AND is_end = 0"
	} else {
		conditionStr += " AND is_end = 1"
	}
	if sort == "episodesUpdateTime" {
		conditionStr += " ORDER BY episodes_update_time DESC"
	} else if sort == "comment" {
		conditionStr += " ORDER BY comment DESC"
	} else {
		conditionStr += " ORDER BY add_time DESC"
	}
	conditionStr += " LIMIT " + limit
	conditionStr += " OFFSET " + offset
	stmt, err := db.Prepare("SELECT id,title,sub_title,img,img1,add_time,episodes_count,is_end FROM video" + conditionStr)
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(channelId)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	videos := make([]*entity.Video, 0)
	var id int64
	var title string
	var subTitle string
	var img string
	var img1 string
	var ctime int64
	var episodesCount int
	var isEnd int
	for rows.Next() {
		rows.Scan(&id, &title, &subTitle, &img, &img1, &ctime, &episodesCount, &isEnd)
		videos = append(videos, &entity.Video{Id: id, Title: title, SubTitle: subTitle, Img: img, Img1: img1, AddTime: ctime, EpisodesCount: episodesCount, IsEnd: isEnd})
	}
	return videos, nil

}

func GetVideoInfo(videoId string) (*entity.Video, error) {
	video := &entity.Video{}
	rediskey := "video:info:" + videoId
	exist, _ := redis.Bool(rdb.Do("exists", rediskey))
	if exist {
		res, _ := redis.Values(rdb.Do("hgetall", rediskey))
		redis.ScanStruct(res, video)
		return video, nil
	}
	stmt, err := db.Prepare("SELECT * FROM video WHERE id = ? limit 1")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	err = stmt.QueryRow(videoId).Scan(&video.Id, &video.Title, &video.SubTitle, &video.Status, &video.AddTime, &video.Img, &video.Img1, &video.ChannelId, &video.TypeId, &video.RegionId, &video.UserId, &video.EpisodesCount, &video.EpisodesUpdateTime, &video.IsEnd, &video.IsHot, &video.IsRecommend, &video.Comment)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}

	_, err = rdb.Do("hmset", redis.Args{rediskey}.AddFlat(video)...)
	if err == nil {
		rdb.Do("expire", rediskey, 86400)
	}
	return video, nil
}

func GetVideoEpisodesList(videoId string) ([]*entity.VideoEpisodes, error) {
	episodesList := make([]*entity.VideoEpisodes, 0)
	rediskey := "video:episodes:" + videoId
	exist, _ := redis.Bool(rdb.Do("exists", rediskey))
	if exist {
		values, _ := redis.Values(rdb.Do("lrange", rediskey, "0", "-1"))

		for _, v := range values {
			episodes := &entity.VideoEpisodes{}
			err := json.Unmarshal(v.([]byte), episodes)
			if err == nil {
				episodesList = append(episodesList, episodes)
			}
		}
		return episodesList, nil
	}
	stmt, err := db.Prepare("SELECT id,title,add_time,num,play_url as playUrl,comment, aliyun_video_id FROM video_episodes WHERE video_id = ? ORDER BY num ASC")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(videoId)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	var id int64
	var title string
	var ctime int64
	var num int
	var playurl string
	var comment int
	var aliyunVideoID string
	for rows.Next() {
		rows.Scan(&id, &title, &ctime, &num, &playurl, &comment, &aliyunVideoID)
		episodesList = append(episodesList, &entity.VideoEpisodes{Id: id, Title: title, AddTime: ctime, Num: num, PlayUrl: playurl, Comment: comment, AliyunVideoId: aliyunVideoID})
	}
	for _, v := range episodesList {
		jsonVal, err := json.Marshal(v)
		if err == nil {
			rdb.Do("rpush", rediskey, jsonVal)
		}
	}
	rdb.Do("expire", rediskey, 86400)
	return episodesList, nil
}

func GetUserVideo(uid string) ([]*entity.Video, error) {
	stmt, err := db.Prepare("SELECT id,title,sub_title,img,img1,add_time,episodes_count,is_end FROM video WHERE user_id = ? ORDER BY add_time DESC")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(uid)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	videos := make([]*entity.Video, 0)
	var id int64
	var title string
	var subTitle string
	var img string
	var img1 string
	var ctime int64
	var episodesCount int
	var isEnd int
	for rows.Next() {
		rows.Scan(&id, &title, &subTitle, &img, &img1, &ctime, &episodesCount, &isEnd)
		videos = append(videos, &entity.Video{Id: id, Title: title, SubTitle: subTitle, Img: img, Img1: img1, AddTime: ctime, EpisodesCount: episodesCount, IsEnd: isEnd})
	}
	return videos, nil
}

func SaveVideo(playUrl, title, subTitle, channelId, typeId, regionId, uid, aliyunVideoId string) error {
	stmt, err := db.Prepare("INSERT INTO video (title, sub_title, status, add_time, img,img1, channel_id, type_id, region_id, user_id, episodes_count, is_end, is_hot, is_recommend, comment) VALUES (?,?,1,?,'','',?,?,?,?,0,1,0,0,0)")
	if err != nil {
		golf.Critical(err)
		return err
	}
	addTime := time.Now().Unix()
	result, err := stmt.Exec(title, subTitle, addTime, channelId, typeId, regionId, uid)
	if err != nil {
		golf.Critical(err)
		return err
	}
	videoId, err := result.LastInsertId()
	if err != nil {
		golf.Critical(err)
		return err
	}
	if aliyunVideoId != "" {
		playUrl = ""
	}
	stmt2, err := db.Prepare("INSERT INTO video_episodes (title, add_time, num, video_id, play_url, status, comment, aliyun_video_id) VALUES (?,?,1,?,?,1,0,?)")
	defer func() {
		stmt.Close()
		stmt2.Close()
	}()
	if err != nil {
		golf.Critical(err)
		return err
	}
	_, err = stmt2.Exec(title, addTime, videoId, playUrl, aliyunVideoId)
	if err != nil {
		golf.Critical(err)
		return err
	}
	return nil
}
