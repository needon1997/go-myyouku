package models

import (
	"database/sql"
	"encoding/json"
	"github.com/gomodule/redigo/redis"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
)

func GetChannelRegion(channelId string) ([]*entity.ChannelInfo, error) {
	stmt, err := db.Prepare("SELECT id,name FROM channel_region WHERE channel_id = ? AND status = 1")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(channelId)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	channelRegion := make([]*entity.ChannelInfo, 0)
	var id int
	var name string
	for rows.Next() {
		rows.Scan(&id, &name)
		channelRegion = append(channelRegion, &entity.ChannelInfo{Id: id, Name: name})
	}
	return channelRegion, nil
}
func GetChannelType(channelId string) ([]*entity.ChannelInfo, error) {
	stmt, err := db.Prepare("SELECT id,name FROM channel_type WHERE channel_id = ? AND status = 1")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(channelId)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	channelRegion := make([]*entity.ChannelInfo, 0)
	var id int
	var name string
	for rows.Next() {
		rows.Scan(&id, &name)
		channelRegion = append(channelRegion, &entity.ChannelInfo{Id: id, Name: name})
	}
	return channelRegion, nil
}

func GetChannelTop(channelId string) ([]*entity.Video, error) {
	rediskey := "channel:top:" + channelId
	videos := make([]*entity.Video, 0)
	exist, _ := redis.Bool(rdb.Do("exists", rediskey))
	if exist {
		vals, _ := redis.Values(rdb.Do("lrange", rediskey, "0", "-1"))
		for _, v := range vals {
			video := &entity.Video{}
			err := json.Unmarshal(v.([]byte), video)
			if err == nil {
				videos = append(videos, video)
			}
		}
		return videos, nil
	}
	stmt, err := db.Prepare("SELECT id,title,sub_title,img,img1,add_time,episodes_count,is_end,comment FROM video WHERE channel_id = ? AND status = 1 ORDER BY comment DESC LIMIT 10")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(channelId)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}

	var id int64
	var title string
	var subTitle string
	var img string
	var img1 string
	var ctime int64
	var episodesCount int
	var isEnd int
	var comment int
	for rows.Next() {
		rows.Scan(&id, &title, &subTitle, &img, &img1, &ctime, &episodesCount, &isEnd, &comment)
		videos = append(videos, &entity.Video{Id: id, Title: title, SubTitle: subTitle, Img: img, Img1: img1, AddTime: ctime, EpisodesCount: episodesCount, IsEnd: isEnd, Comment: comment})
	}
	for _, v := range videos {
		jsonByte, err := json.Marshal(v)
		if err == nil {
			rdb.Do("rpush", rediskey, jsonByte)
		}
	}
	rdb.Do("expire", rediskey, 86400)
	return videos, nil
}

func GetTypeTop(typeId string) ([]*entity.Video, error) {
	rediskey := "type:top:" + typeId
	videos := make([]*entity.Video, 0)
	exist, _ := redis.Bool(rdb.Do("exists", rediskey))
	if exist {
		vals, _ := redis.Values(rdb.Do("lrange", rediskey, "0", "-1"))
		for _, v := range vals {
			video := &entity.Video{}
			err := json.Unmarshal(v.([]byte), video)
			if err == nil {
				videos = append(videos, video)
			}
		}
		return videos, nil
	}
	stmt, err := db.Prepare("SELECT id,title,sub_title,img,img1,add_time,episodes_count,is_end, comment FROM video WHERE type_id = ? AND status = 1 ORDER BY comment DESC LIMIT 10")
	defer func() { stmt.Close() }()
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	rows, err := stmt.Query(typeId)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		golf.Critical(err)
		return nil, err
	}
	var id int64
	var title string
	var subTitle string
	var img string
	var img1 string
	var ctime int64
	var episodesCount int
	var isEnd int
	var comment int
	for rows.Next() {
		rows.Scan(&id, &title, &subTitle, &img, &img1, &ctime, &episodesCount, &isEnd, &comment)
		videos = append(videos, &entity.Video{Id: id, Title: title, SubTitle: subTitle, Img: img, Img1: img1, AddTime: ctime, EpisodesCount: episodesCount, IsEnd: isEnd, Comment: comment})
	}
	for _, v := range videos {
		jsonByte, err := json.Marshal(v)
		if err == nil {
			rdb.Do("rpush", rediskey, jsonByte)
		}
	}
	rdb.Do("expire", rediskey, 86400)
	return videos, nil
}
