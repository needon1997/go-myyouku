package models

import (
	"github.com/needon1997/go-golf"
	"github.com/streadway/amqp"
)

var connectionStr string

type Callback func(msg string) error

func getConnection() (*amqp.Connection, error) {
	return amqp.Dial(golf.App.Config.String("mq_connection_string"))
}

func Publish(exchange string, queueName string, msg string) error {
	conn, err := getConnection()
	if err != nil {
		golf.Critical(err)
		return err
	}
	ch, err := conn.Channel()
	if err != nil {
		golf.Critical(err)
		return err
	}
	q, err := ch.QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		golf.Critical(err)
		return err
	}
	err = ch.Publish(exchange, q.Name, false, false, amqp.Publishing{DeliveryMode: amqp.Persistent, ContentType: "text/plain", Body: []byte(msg)})
	return err
}

func Consume(exchange string, queueName string, callback Callback) {
	conn, err := getConnection()
	if err != nil {
		golf.Critical(err)
		return
	}
	ch, err := conn.Channel()
	if err != nil {
		golf.Critical(err)
		return
	}
	q, err := ch.QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		golf.Critical(err)
		return
	}
	msgs, err := ch.Consume(q.Name, "", false, false, false, false, nil)
	if err != nil {
		golf.Critical(err)
		return
	}
	for msg := range msgs {
		s := string(msg.Body)
		err = callback(s)
		if err == nil {
			msg.Ack(false)
		}
	}
}
