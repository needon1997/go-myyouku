module github.com/needon1997/go-myyouku

go 1.15

require (
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.1060
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gomodule/redigo v1.8.4
	github.com/gorilla/websocket v1.4.2
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/needon1997/go-golf v1.1.2
	github.com/streadway/amqp v1.0.0
	gopkg.in/ini.v1 v1.62.0 // indirect
)
