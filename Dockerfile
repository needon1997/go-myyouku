FROM golang:latest
RUN go get -u github.com/beego/bee
ENV GO111MODULE=on
ENV GOFLAGS=-mod=vendor
ENV APP_HOME /go/src/myvideoapp
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
EXPOSE 10080
CMD ["bee", "run"]