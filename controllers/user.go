package controllers

import (
	"fmt"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"github.com/needon1997/go-myyouku/models"
	"strconv"
	"strings"
)

type UserController struct {
	golf.Controller
}

func (this *UserController) Init(prefix string) {
	this.Controller.Init(prefix)
	this.PostFuncMapping("/login", login)
	this.PostFuncMapping("/register", register)
	this.PostFuncMapping("/sendmsg", sendmsg)
}

func login(ctx *golf.Context) {
	mobile, _ := ctx.GetFormValue("mobile")
	password, _ := ctx.GetFormValue("password")
	if mobile == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "mobile cannot be empty", Code: 5000})
		return
	}
	if password == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "password cannot be empty", Code: 5000})
		return
	}
	if !isMobile(mobile) {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "mobile format incorrect", Code: 5000})
		return
	}
	result, err := models.UserLogin(mobile, cryptoPassword(password))
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if result == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "password or mobile number error", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: result, Count: 1})
}

func register(ctx *golf.Context) {
	mobile, _ := ctx.GetFormValue("mobile")
	password, _ := ctx.GetFormValue("password")
	if mobile == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "mobile cannot be empty", Code: 5000})
		return
	}
	if password == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "password cannot be empty", Code: 5000})
		return
	}
	if !isMobile(mobile) {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "mobile format incorrect", Code: 5000})
		return
	}
	if result, err := models.SearchMobile(mobile); err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	} else if result {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "mobile already exist", Code: 5000})
		return
	}
	err := models.UserSave(mobile, cryptoPassword(password))
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: "login success", Count: 1})
}

func sendmsg(ctx *golf.Context) {
	uids, _ := ctx.GetFormValue("uids")
	content, _ := ctx.GetFormValue("content")
	if uids == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "uid cannot be empty", Code: 5000})
		return
	}
	if content == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "content cannot be empty", Code: 5000})
		return
	}
	uid_arr := strings.Split(uids, ",")
	msg_id, err := models.SaveMessage(content)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if msg_id != -1 {
		go func() {
			for _, uidStr := range uid_arr {
				uid, err := strconv.Atoi(uidStr)
				if err != nil {
					ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "user id format error", Code: 5005})
					return
				}
				models.Publish("", "message", fmt.Sprintf("%v:%v", uid, msg_id))
			}
		}()
		ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: "msg send success", Count: 1})
		return
	} else {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}

}
