package controllers

import (
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk"
	credentials2 "github.com/aliyun/alibaba-cloud-sdk-go/sdk/auth/credentials"
	vod2 "github.com/aliyun/alibaba-cloud-sdk-go/services/vod"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
)

type AliCloudController struct {
	golf.Controller
}
type AliResponseData struct {
	RequestId     string
	UploadAddress string
	UploadAuth    string
	VideoId       string
}

func (this *AliCloudController) Init(prefix string) {
	this.Controller.Init(prefix)
	this.PostFuncMapping("/create/upload/video", createUploadVideo)
	this.PostFuncMapping("/refresh/upload/video", refreshUploadVideo)
	this.PostFuncMapping("/video/play/auth", getPlayAuth)
}
func initVodClient(accessKeyId string, accessKeySecret string) (client *vod2.Client, err error) {
	regionId := "eu-central-1"
	credential := &credentials2.AccessKeyCredential{
		accessKeyId,
		accessKeySecret,
	}
	config := sdk.NewConfig()
	config.AutoRetry = true     // 失败是否自动重试
	config.MaxRetryTime = 3     // 最大重试次数
	config.Timeout = 3000000000 // 连接超时，单位：纳秒；默认为3秒
	return vod2.NewClientWithOptions(regionId, config, credential)
}

func createUploadVideo(ctx *golf.Context) {
	request := vod2.CreateCreateUploadVideoRequest()
	request.Title, _ = ctx.GetFormValue("title")
	request.Description, _ = ctx.GetFormValue("desc")
	request.FileName, _ = ctx.GetFormValue("fileName")
	request.CoverURL, _ = ctx.GetFormValue("coverUrl")
	request.Tags, _ = ctx.GetFormValue("tags")
	request.AcceptFormat = "JSON"
	client, err := initVodClient(golf.App.Config.String("access_key"), golf.App.Config.String("access_secret"))
	if err != nil {
		golf.Critical(err)
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "alicloud client request error", Code: 5005})
		return
	}
	rsp, err := client.CreateUploadVideo(request)
	if err != nil {
		golf.Critical(err)
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "alicloud client request error", Code: 5005})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: &AliResponseData{RequestId: rsp.RequestId, UploadAddress: rsp.UploadAddress, UploadAuth: rsp.UploadAuth, VideoId: rsp.VideoId}, Count: 1})
}
func refreshUploadVideo(ctx *golf.Context) {
	request := vod2.CreateRefreshUploadVideoRequest()
	request.VideoId, _ = ctx.GetFormValue("VideoId")
	request.AcceptFormat = "JSON"
	client, err := initVodClient(golf.App.Config.String("access_key"), golf.App.Config.String("access_secret"))
	if err != nil {
		golf.Critical(err)
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "alicloud client request error", Code: 5005})
		return
	}
	rsp, err := client.RefreshUploadVideo(request)
	if err != nil {
		golf.Critical(err)
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "alicloud client request error", Code: 5005})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: &AliResponseData{RequestId: rsp.RequestId, UploadAddress: rsp.UploadAddress, UploadAuth: rsp.UploadAuth, VideoId: rsp.VideoId}, Count: 1})
}

func getPlayAuth(ctx *golf.Context) {
	request := vod2.CreateGetVideoPlayAuthRequest()
	request.VideoId, _ = ctx.GetFormValue("videoId")
	request.AcceptFormat = "JSON"
	client, err := initVodClient(golf.App.Config.String("access_key"), golf.App.Config.String("access_secret"))
	if err != nil {
		golf.Critical(err)
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "alicloud client request error", Code: 5005})
		return
	}
	rsp, err := client.GetVideoPlayAuth(request)
	if err != nil {
		golf.Critical(err)
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "alicloud client request error", Code: 5005})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: rsp.PlayAuth, Count: 1})
}
