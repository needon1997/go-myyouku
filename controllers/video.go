package controllers

import (
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"github.com/needon1997/go-myyouku/models"
)

type VideoController struct {
	golf.Controller
}

func (this *VideoController) Init(prefix string) {
	this.Controller.Init(prefix)
	this.GetFuncMapping("/channel/advert", channelAdvert)
	this.GetFuncMapping("/channel/hot", channelHotList)
	this.GetFuncMapping("/channel/recommend/region", channelRegionRecommendList)
	this.GetFuncMapping("/channel/recommend/type", channelTypeRecommendList)
	this.GetFuncMapping("/channel", channelVideo)
	this.PostFuncMapping("/search", searchVideo)
	this.GetFuncMapping("/userv", userVideo)
	this.PostFuncMapping("/save", saveVideo)
	this.GetFuncMapping("/info", videoInfo)
	this.GetFuncMapping("/episodes/list", videoEpisodesList)
}

//  /video/channel/advert [get]
func channelAdvert(ctx *golf.Context) {
	channelId, _ := ctx.GetFormValue("channelId")
	if channelId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	adverts, err := models.GetChannelAdvert(channelId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if adverts == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: adverts, Count: int64(len(adverts))})
}

// /video/channel/hot [get]
func channelHotList(ctx *golf.Context) {
	channelId, _ := ctx.GetFormValue("channelId")
	if channelId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	hotlist, err := models.GetChannelHotList(channelId, 9)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if hotlist == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: hotlist, Count: int64(len(hotlist))})
}

// @router /video/channel/recommend/region [get]
func channelRegionRecommendList(ctx *golf.Context) {
	channelId, _ := ctx.GetFormValue("channelId")
	regionId, _ := ctx.GetFormValue("regionId")
	if channelId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	if regionId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "region Id cannot be empty", Code: 5000})
		return
	}
	recommendList, err := models.GetRecommendRegionList(channelId, regionId, 9)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if recommendList == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: recommendList, Count: int64(len(recommendList))})
}

// @router /video/channel/recommend/type [get]
func channelTypeRecommendList(ctx *golf.Context) {
	channelId, _ := ctx.GetFormValue("channelId")
	typeId, _ := ctx.GetFormValue("typeId")
	if channelId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	if typeId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "type Id cannot be empty", Code: 5000})
		return
	}
	recommendList, err := models.GetRecommendTypeList(channelId, typeId, 9)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if recommendList == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: recommendList, Count: int64(len(recommendList))})

}

// @router /video/channel [get]
func channelVideo(ctx *golf.Context) {
	channelId, _ := ctx.GetFormValue("channelId")
	if channelId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	regionId, _ := ctx.GetFormValue("regionId")
	typeId, _ := ctx.GetFormValue("typeId")
	end, _ := ctx.GetFormValue("end")
	sort, _ := ctx.GetFormValue("sort")
	limit, _ := ctx.GetFormValue("limit")
	offset, _ := ctx.GetFormValue("offset")
	if limit == "" {
		limit = "100"
	}
	if offset == "" {
		offset = "0"
	}
	videos, err := models.GetChannelVideoList(channelId, regionId, typeId, end, sort, limit, offset)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if videos == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: videos, Count: int64(len(videos))})
}

// @router /video/search [post]
func searchVideo(ctx *golf.Context) {

}

// @router /video/userv [get]
func userVideo(ctx *golf.Context) {
	uid, _ := ctx.GetFormValue("uid")
	if uid == "0" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "user Id cannot be empty", Code: 5000})
		return
	}
	videoList, err := models.GetUserVideo(uid)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "user Id cannot be empty", Code: 5000})
		return
	}
	if videoList == nil {
		ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: videoList, Count: 0})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: videoList, Count: int64(len(videoList))})
}

func saveVideo(ctx *golf.Context) {
	playurl, _ := ctx.GetFormValue("playUrl")
	title, _ := ctx.GetFormValue("title")
	subTitile, _ := ctx.GetFormValue("subTitle")
	channelId, _ := ctx.GetFormValue("channelId")
	typeId, _ := ctx.GetFormValue("typeId")
	regionId, _ := ctx.GetFormValue("regionId")
	uid, _ := ctx.GetFormValue("uid")
	aliyunVideoId, _ := ctx.GetFormValue("aliyunVideoId")
	if uid == "0" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "user Id cannot be empty", Code: 5000})
		return
	}
	if playurl == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "playurl cannot be empty", Code: 5000})
		return
	}
	if title == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "title cannot be empty", Code: 5000})
		return
	}
	err := models.SaveVideo(playurl, title, subTitile, channelId, typeId, regionId, uid, aliyunVideoId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: "upload success", Count: 1})
}

// @router /video/info [get]
func videoInfo(ctx *golf.Context) {
	videoId, _ := ctx.GetFormValue("videoId")
	if videoId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "video Id cannot be empty", Code: 5000})
		return
	}
	video, err := models.GetVideoInfo(videoId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if video == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: video, Count: 1})

}

// @router /video/episodes/list [get]
func videoEpisodesList(ctx *golf.Context) {
	videoId, _ := ctx.GetFormValue("videoId")
	if videoId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "video Id cannot be empty", Code: 5000})
		return
	}
	videoepisodes, err := models.GetVideoEpisodesList(videoId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if videoepisodes == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: videoepisodes, Count: 1})
}
