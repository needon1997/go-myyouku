package controllers

import (
	"crypto/md5"
	"encoding/hex"
	golf "github.com/needon1997/go-golf"
	"regexp"
	"time"
)

const mobileFormat = "^([1-9]{1})([0-9]{9})$"

var mobileRegx *regexp.Regexp

func init() {
	mobileRegx, _ = regexp.Compile(mobileFormat)
}
func isMobile(mobile string) bool {
	return mobileRegx.MatchString(mobile)
}

func cryptoPassword(password string) string {
	m := md5.New()
	m.Write([]byte(password + golf.App.Config.String("MD5_V")))
	return hex.EncodeToString(m.Sum(nil))
}

func timeformat(t int64) string {
	timeFormat := time.Unix(t, 0)
	return timeFormat.Format("2006-01-02")
}
