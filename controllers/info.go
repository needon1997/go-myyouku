package controllers

import (
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"github.com/needon1997/go-myyouku/models"
)

type InfoController struct {
	golf.Controller
}

func (this *InfoController) Init(prefix string) {
	this.Controller.Init(prefix)
	this.GetFuncMapping("/channel/region", channelRegionInfo)
	this.GetFuncMapping("/channel/type", channelTypeInfo)
	this.GetFuncMapping("/channel/top", channelTopInfo)
	this.GetFuncMapping("/type/top", typeTopInfo)
}

func channelRegionInfo(ctx *golf.Context) {
	channelId, _ := ctx.GetFormValue("channelId")
	if channelId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	channelRegion, err := models.GetChannelRegion(channelId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if channelRegion == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: channelRegion, Count: int64(len(channelRegion))})

}

func channelTypeInfo(ctx *golf.Context) {
	channelId, _ := ctx.GetFormValue("channelId")
	if channelId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	channelType, err := models.GetChannelType(channelId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if channelType == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: channelType, Count: int64(len(channelType))})
}

func channelTopInfo(ctx *golf.Context) {
	channelId, _ := ctx.GetFormValue("channelId")
	if channelId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	top10Videos, err := models.GetChannelTop(channelId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if top10Videos == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: top10Videos, Count: 1})
}

func typeTopInfo(ctx *golf.Context) {
	typeId, _ := ctx.GetFormValue("typeId")
	if typeId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "chanel Id cannot be empty", Code: 5000})
		return
	}
	top10Videos, err := models.GetTypeTop(typeId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if top10Videos == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: top10Videos, Count: int64(len(top10Videos))})
}
