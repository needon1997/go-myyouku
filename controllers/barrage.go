package controllers

import (
	"encoding/json"
	websocket2 "github.com/gorilla/websocket"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"github.com/needon1997/go-myyouku/models"
	"net/http"
)

type BarrageController struct {
	golf.Controller
}

var upgrader websocket2.Upgrader

func init() {
	upgrader = websocket2.Upgrader{CheckOrigin: func(r *http.Request) bool {
		return true
	}}
}
func (this *BarrageController) Init(prefix string) {
	this.Controller.Init(prefix)
	this.PostFuncMapping("/save", saveBarrage)
	this.GetFuncMapping("/ws", listBarrage)
}

// /barrage/save [post]
func saveBarrage(ctx *golf.Context) {
	episodesId, _ := ctx.GetFormValue("episodesId")
	videoId, _ := ctx.GetFormValue("videoId")
	uid, _ := ctx.GetFormValue("uid")
	content, _ := ctx.GetFormValue("content")
	currentTime, _ := ctx.GetFormValue("currentTime")
	if content == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "content cannot be empty", Code: 5000})
		return
	}
	if uid == "0" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "please login ", Code: 5000})
		return
	}
	if episodesId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "please select episodes ", Code: 5000})
		return
	}
	if videoId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "please select video", Code: 5000})
		return
	}
	if currentTime == "0" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "please select time", Code: 5000})
		return
	}
	err := models.SaveBarrage(content, uid, episodesId, videoId, currentTime)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: "barrage publish success", Count: 1})
}

// /barrage/ws [get]
func listBarrage(ctx *golf.Context) {
	conn, err := upgrader.Upgrade(ctx.W, ctx.R, nil)
	if err != nil {
		goto ERR
	}
	for {
		_, data, err := conn.ReadMessage()
		if err != nil {
			goto ERR
		}
		var wsData entity.WsData
		json.Unmarshal(data, &wsData)
		endTime := wsData.CurrentTime + 60
		barrages, err := models.GetBarrage(wsData.EpisodesId, wsData.CurrentTime, endTime)
		if err == nil {
			if err = conn.WriteJSON(barrages); err != nil {
				goto ERR
			}
		}
	}
ERR:
	conn.Close()
}
