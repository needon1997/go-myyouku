package controllers

import (
	"fmt"
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/entity"
	"github.com/needon1997/go-myyouku/models"
)

type CommentController struct {
	golf.Controller
}

func (this *CommentController) Init(prefix string) {
	this.Controller.Init(prefix)
	this.PostFuncMapping("/save", saveComment)
	this.GetFuncMapping("/list", listComment)
}

// /comment/save [post]
func saveComment(ctx *golf.Context) {
	content, _ := ctx.GetFormValue("content")
	uid, _ := ctx.GetFormValue("uid")
	episodesId, _ := ctx.GetFormValue("episodesId")
	videoId, _ := ctx.GetFormValue("videoId")
	if content == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "content cannot be empty", Code: 5000})
		return
	}
	if uid == "0" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "please login ", Code: 5000})
		return
	}
	if episodesId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "please select episodes ", Code: 5000})
		return
	}
	if videoId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "please select video", Code: 5000})
		return
	}
	err := models.SaveComment(content, uid, episodesId, videoId)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: "comment success", Count: 1})

}

// /comment/list [get]
func listComment(ctx *golf.Context) {
	episodesId, _ := ctx.GetFormValue("episodesId")
	limit, _ := ctx.GetFormValue("limit")
	offset, _ := ctx.GetFormValue("offset")
	if limit == "" || limit == "0" {
		limit = "12"
	}
	if offset == "" {
		offset = "0"
	}
	if episodesId == "" {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "Episode Id cannot be empty", Code: 5000})
		return
	}
	commentList, err := models.GetEpisodeComment(episodesId, limit, offset)
	if err != nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "database internal error", Code: 5005})
		return
	}
	if commentList == nil {
		ctx.SetResponseJSON(200, &entity.ErrorResponse{Msg: "no content", Code: 5000})
		return
	}
	userInfoMap := make(map[int]*entity.UserInfo)
	for i := 0; i < len(commentList); i++ {
		commentList[i].AddTimeTitle = timeformat(commentList[i].AddTime)
		if userInfo, ok := userInfoMap[commentList[i].UserId]; ok {
			commentList[i].UserInfo = userInfo
		} else {
			commentList[i].UserInfo, _ = models.GetUserInfo(fmt.Sprint(commentList[i].UserId))
			userInfoMap[commentList[i].UserId] = commentList[i].UserInfo
		}
	}
	ctx.SetResponseJSON(200, &entity.SuccessResponse{Code: 0, Items: commentList, Count: int64(len(commentList))})

}
