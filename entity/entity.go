package entity

type SuccessResponse struct {
	Code  int         `json:"code"`
	Items interface{} `json:"items"`
	Count int64       `json:"count"`
}
type ErrorResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type User struct {
	Id      int64 `json:"uid"`
	Name    string
	Mobile  string
	AddTime int64
	Status  int
	Avatar  string
}
type UserInfo struct {
	Id      int64  `json:"id"`
	Name    string `json:"name"`
	AddTime int64  `json:"addTime"`
	Avatar  string `json:"avatar"`
}

type Message struct {
	UserId    int64
	MessageId int64
	Status    int
	AddTime   int64
}

type Advert struct {
	Id       int64
	Title    string
	SubTitle string
	Img      string
	AddTime  int64
	Url      string
	Status   int
}

type Video struct {
	Id                 int64
	Title              string
	SubTitle           string
	Status             int
	AddTime            int64
	Img                string
	Img1               string
	ChannelId          int
	TypeId             int
	RegionId           int
	UserId             int
	EpisodesCount      int
	EpisodesUpdateTime int
	IsEnd              int
	IsHot              int
	IsRecommend        int
	Comment            int
}

type VideoEpisodes struct {
	Id            int64
	Title         string
	AddTime       int64
	Num           int
	VideoId       int64
	PlayUrl       string
	Status        int
	Comment       int
	AliyunVideoId string
}

type ChannelInfo struct {
	Id        int
	Name      string
	Status    int
	AddTime   int64
	ChannelId int
	Sort      int
}
type Comment struct {
	Id           int64
	Content      string
	AddTime      int64
	AddtimeTitle string
	UserId       int64
	Status       int
	Stamp        int
	PraiseCount  int
	EpisodesId   int64
	VideoId      int64
}
type CommentInfo struct {
	Id           int64     `json:"id"`
	Content      string    `json:"content"`
	AddTime      int64     `json:"addTime"`
	AddTimeTitle string    `json:"addTimeTitle"`
	UserId       int       `json:"userId"`
	Stamp        int       `json:"stamp"`
	PraiseCount  int       `json:"praiseCount"`
	UserInfo     *UserInfo `json:"userinfo"`
}
type Barrage struct {
	Id          int    `json:"id"`
	Content     string `json:"content"`
	CurrentTime int    `json:"currentTime"`
	AddTime     int
	UserId      int
	Status      int
	EpisodesId  int
	VideoId     int
}
type WsData struct {
	EpisodesId  int
	CurrentTime int
}
