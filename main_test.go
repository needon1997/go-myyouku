package main_test

import (
	"fmt"
	"github.com/needon1997/go-myyouku/models"
	"testing"
	"time"
)

func TestPublishAndConsume(t *testing.T) {
	for i := 0; i < 10; i++ {
		go models.Publish("", "video:comment", fmt.Sprint(i))
		go models.Publish("", "episodes:comment", fmt.Sprint(i))
		go models.Publish("", "message", fmt.Sprintf("%v:%v", i, i))
		time.Sleep(1 * time.Second)
	}

}
