package main

import (
	golf "github.com/needon1997/go-golf"
	"github.com/needon1997/go-myyouku/controllers"
	"github.com/needon1997/go-myyouku/models"
)

func main() {
	golf.App.Add("/barrage", &controllers.BarrageController{})
	golf.App.Add("/video", &controllers.VideoController{})
	golf.App.Add("/comment", &controllers.CommentController{})
	golf.App.Add("/user", &controllers.UserController{})
	golf.App.Add("/info", &controllers.InfoController{})
	golf.App.Add("/alicloud", &controllers.AliCloudController{})
	golf.App.RegisterDaemonGo(models.ConsumeComment)
	golf.App.RegisterDaemonGo(models.ConsumeSendMessage)
	golf.App.Run()
}
